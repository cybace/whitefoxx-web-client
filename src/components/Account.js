import React, { Component } from 'react';

class Account extends Component {
    render() {
        var action = this.props.match.params.action;
        if (!action) {
            console.log('SHOW ACCOUNT');
        } else if (action === 'login') {
            console.log('LOGIN');
        } else if (action === 'signup') {
            console.log('SIGNUP');
        }

        return (
            <div>
                <h2>/account/{action}</h2>
            </div>
        );
    }
}

export default Account;
