import React, { Component } from 'react';

class Categories extends Component {
    render() {
        var category = this.props.match.params.category;
        if (category) {
            console.log('CATEGORY EXISTS');
        } else {
            console.log('CATEGORY DOES NOT EXIST');
        }
        return (
            <div>
                <h2>/categories/{category}</h2>
            </div>
        );
    }
}

export default Categories;
