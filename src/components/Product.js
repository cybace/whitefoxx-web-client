import React, { Component } from 'react';

class Product extends Component {
    render() {
        var category = this.props.match.params.category;
        var product = this.props.match.params.product;
        return (
            <div>
                <h2>/categories/{category}/products/{product}</h2>
            </div>
        );
    }
}

export default Product;
