import React, { Component } from 'react';

class Search extends Component {
    render() {
        var query = this.props.match.params.query;
        if (query) {
            console.log('SEARCH TERM EXISTS');
        } else {
            console.log('SEARCH TERM DOES NOT EXIST');
        }
        return (
            <div>
                <h2>/search/{query}</h2>
            </div>
        );
    }
}

export default Search;
