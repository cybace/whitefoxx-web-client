import AboutUs from './components/AboutUs.js';
import Account from './components/Account.js';
import Cart from './components/Cart.js';
import Categories from './components/Categories.js';
import ContactUs from './components/ContactUs.js';
import Copyright from './components/Copyright.js';
import Home from './components/Home.js';
import PrivacyPolicy from './components/PrivacyPolicy.js';
import Product from './components/Product.js';
import RefundsPolicy from './components/RefundsPolicy.js';
import Search from './components/Search.js';
import TermsAndConditions from './components/TermsAndConditions.js';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <div>
            <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/search/:query?' exact component={Search} />
                <Route path='/categories/:category?' exact component={Categories} />
                <Route path='/categories/:category/products/:product' exact component={Product} />
                <Route path='/cart' exact component={Cart} />
                <Route path='/account/:action?' exact component={Account} />
                <Route path='/about-us' exact component={AboutUs} />
                <Route path='/contact-us' exact component={ContactUs} />
                <Route path='/copyright' exact component={Copyright} />
                <Route path='/privacy-policy' exact component={PrivacyPolicy} />
                <Route path='/refunds-policy' exact component={RefundsPolicy} />
                <Route path='/terms-and-conditions' exact component={TermsAndConditions} />
                <Route path='*' component={Home} />
            </Switch>
            </div>
        );
    }
}

export default App;
